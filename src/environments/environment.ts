// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
   firebase: {
    apiKey: "AIzaSyDbj_5cXiNSHLWfNU2kPoZ_36IRA3eu7OI",
    authDomain: "my-project-1565243588474.firebaseapp.com",
    databaseURL: "https://my-project-1565243588474-default-rtdb.firebaseio.com",
    projectId: "my-project-1565243588474",
    storageBucket: "my-project-1565243588474.appspot.com",
    messagingSenderId: "480038738139",
    appId: "1:480038738139:web:876c410910732028d814fe",
    measurementId: "G-0W9LBXX6XC"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
