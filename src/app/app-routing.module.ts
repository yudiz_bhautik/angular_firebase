import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FpasswordComponent } from './page/fpassword/fpassword.component';
import { HomeComponent } from './page/home/home.component';
import { LoginComponent } from './page/login/login.component';
import { SignupComponent } from './page/signup/signup.component';

import { AuthGuard } from "./auth/auth.guard";
import { BookListComponent } from './page/home/book-list/book-list.component';
import { AddBookComponent } from './page/home/add-book/add-book.component';
import { EditBookComponent } from './page/home/edit-book/edit-book.component';

const routes: Routes = [
  {path:'',component:LoginComponent},
  {path:'login',component:LoginComponent},
  {path:'signup',component:SignupComponent},
  {path:'home',component:HomeComponent ,canActivate: [AuthGuard] ,children:[
    { path: '', pathMatch: 'full', redirectTo: 'add-book' },
    { path: 'add-book', component: AddBookComponent },
    { path: 'edit-book/:id', component: EditBookComponent },
    { path: 'books-list', component: BookListComponent }
    
  ]},
  {path:'fpass',component:FpasswordComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
