import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { Location } from '@angular/common';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatChipInputEvent } from '@angular/material/chips';
import { ServiceService } from './../../../auth/service.service';


import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { AngularFireStorage } from '@angular/fire/storage';

export interface Language {
  name: string;
}

@Component({
  selector: 'app-edit-book',
  templateUrl: './edit-book.component.html',
  styleUrls: ['./edit-book.component.scss']
})

export class EditBookComponent implements OnInit {
  [x: string]: any;
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  selected:any
  @ViewChild('chipList') chipList:any;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  selectedBindingType!: string;
  editBookForm!: FormGroup;
  BindingType: any = ['Paperback', 'Case binding', 'Perfect binding', 'Saddle stitch binding', 'Spiral binding'];

  ngOnInit() {
    this.updateBookForm();
  }

  constructor(
    public fb: FormBuilder,    
    private location: Location,
    private bookApi: ServiceService,
    private actRoute: ActivatedRoute,
    private storage:AngularFireStorage
  ) { 
    var id = this.actRoute.snapshot.paramMap.get('id');
    this.bookApi.GetBook(id!).valueChanges().subscribe(data => {
      this.languageArray = data.languages;
      this.editBookForm.setValue(data);
    })
  }

  /* Update form */
  updateBookForm(){
    this.editBookForm = this.fb.group({
      book_name: ['', [Validators.required]],
      isbn_10: ['', [Validators.required]],
      author_name: ['', [Validators.required]],
      publication_date: ['', [Validators.required]],
      binding_type: ['', [Validators.required]],
      in_stock: ['Yes'],
      languages: [''],
      image:['',Validators.required]
    })
  }

  uploadFile(e:any){
    const file = e.target!.files[0];
    console.log(file)
    const filePath =`images/${file.name}`;
    this.img.setValue(filePath)
    const ref = this.storage.ref(filePath);
    const task = ref.put(file);
  
  }
  get img(){
    return this.editBookForm.controls['image'];
  }

  /* Add language */
  add(event: MatChipInputEvent): void {
    var input: any = event.input;
    var value: any = event.value;
    // Add language
    if ((value || '').trim() && this.languageArray.length < 5) {
      this.languageArray.push({name: value.trim()});
    }
    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  /* Remove language */
  remove(language: any): void {
    const index = this.languageArray.indexOf(language);
    if (index >= 0) {
      this.languageArray.splice(index, 1);
    }
  }

  /* Get errors */
  public handleError = (controlName: string, errorName: string) => {
    return this.editBookForm.controls[controlName].hasError(errorName);
  }

  /* Date */
  formatDate(e:any) {
    var convertDate = new Date(e.target.value).toISOString().substring(0, 10);
    this.editBookForm.get('publication_date')!.setValue(convertDate, {
      onlyself: true
    })
  }




  /* Go to previous page */
  goBack(){
    this.location.back();
  }

  /* Submit book */
  updateBook() {
    var id = this.actRoute.snapshot.paramMap.get('id');
    if(window.confirm('Are you sure you wanna update?')){
        this.bookApi.UpdateBook(id!, this.editBookForm.value);
    }
  }

}