import { Component, HostListener, OnInit, ViewChild } from '@angular/core';
// import { MatSidenav } from '@angular/material/sidenav';
import { AuthService } from './../../auth/auth.service';
// import { SidebarComponent } from '@syncfusion/ej2-angular-navigations';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  showFiller = false;
  data:any = ''
  public isMenuOpen: boolean = false;

  constructor(private auth:AuthService) {
      this.data = JSON.parse(localStorage.getItem('user')!);
   }
  ngOnInit() {
  }
  signout(){  
    if(window.confirm('Are you sure you wanna Logout?')){

    this.auth.singout();
    }
  }
 
 
  public onSidenavClick(): void {
  this.isMenuOpen = false;
}

}
