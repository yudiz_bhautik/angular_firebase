import { Book } from './../../../auth/data';
import { Component, ViewChild } from '@angular/core';
import { MatPaginator} from '@angular/material/paginator';
import { ServiceService } from './../../../auth/service.service';

import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.scss']
})

export class BookListComponent {
  
  dataSource!: MatTableDataSource<Book>;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  BookData: any = [];
  displayedColumns: any[] = [
    '$key',
    'img',
    'book_name',
    'author_name', 
    'publication_date',
    'in_stock',
    'action'
  ];
  
  constructor(private bookApi: ServiceService){
   const a  = this.bookApi.GetBookList()!
    .snapshotChanges().subscribe((books:any) => {
        books.forEach((item:any) => {
          let a = item.payload.toJSON();
          a['$key'] = item.key;
          this.BookData.push(a as Book)
        })
        console.log(this.BookData);
        this.dataSource = new MatTableDataSource(this.BookData);
        setTimeout(() => {
          this.dataSource.paginator = this.paginator;
        }, 0);
      })
      
    }


  geturl(e:any){
   const a = e.split('/')
    return "https://firebasestorage.googleapis.com/v0/b/my-project-1565243588474.appspot.com/o/"+a[0]+"%2F"+a[1]+"?alt=media";
    
  }


  /* Delete */
  deleteBook(index: number, e:any){
    if(window.confirm('Are you sure?')) {
      const data = this.dataSource.data;
      data.splice((this.paginator.pageIndex * this.paginator.pageSize) + index, 1);
      this.dataSource.data = data;
      this.bookApi.DeleteBook(e.$key)
    }
  }
  
}