import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { AuthService } from './../../auth/auth.service';
import { User  } from '../../auth/data';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  hide = true;
  userdata:any=[]
  loginform:FormGroup
  constructor(private fb:FormBuilder,public auth2: AngularFireAuth,private _snackBar:MatSnackBar, private router:Router, public auth:AuthService) {
    this.loginform = this.fb.group({
      Email:['',[Validators.required,Validators.pattern('^[a-z0-9](\.?[a-z0-9]){5,}@g(oogle)?mail\.com$')]],
      Password:['',[Validators.required,Validators.pattern('^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{7,15}$')]],
     
    })


        this.auth.getdata().subscribe(data => {
          this.userdata = data.map(e => {
            return {
              id: e.payload.doc.id,
              data: e.payload.doc.data() as User
            };
          })
    
        });
   }


  logout() {
    this.auth2.signOut();
  }
    glogin(){
      this.auth.googlelogin()
    }
  public handleError = (controlName: string, errorName: string) => {
    return this.loginform.controls[controlName].hasError(errorName);
  }


   login(){
     if(this.loginform.valid){
    this.auth.login(this.loginform.controls['Email'].value , this.loginform.controls['Password'].value).then(data=>{
       this.router.navigate(['/home']).then(()=>{
        this.openSnackBar('You are login succesfully ')
        //  console.log(data)
       })

   }).catch(error=>{
     alert(error)
   });
  }
  }


  openSnackBar(message:string) {
    this._snackBar.open(message,"ok", {
      duration: 1000,
    });
  }


  ngOnInit(): void {
  }

}
