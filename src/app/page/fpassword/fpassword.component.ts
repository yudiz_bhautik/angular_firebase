import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { AuthService } from './../../auth/auth.service';


@Component({
  selector: 'app-fpassword',
  templateUrl: './fpassword.component.html',
  styleUrls: ['./fpassword.component.scss']
})
export class FpasswordComponent implements OnInit {

  hide = true;
  loginform:FormGroup
  constructor(private fb:FormBuilder,private _snackBar:MatSnackBar, private router:Router, public auth:AuthService) {
    this.loginform = this.fb.group({
      Password: ['',[Validators.required,Validators.maxLength(8)]],
      Cpassword:['',[Validators.required,Validators.maxLength(8)]]
    })
   }

   password(){

   }
  ngOnInit(): void {
  }

}
