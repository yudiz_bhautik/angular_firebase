import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { AuthService } from '../../auth/auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  hide = true;
signup:FormGroup;
user:any;
  error: any;

  constructor(private fb:FormBuilder,private Af:AngularFirestore,private _snackBar:MatSnackBar,private router:Router, private auth:AuthService) {
    this.signup = this.fb.group({
      username: ['',[Validators.required,Validators.minLength(3),Validators.pattern('^[a-zA-Z0-9](_(?!(\.|_))|\.(?!(_|\.))|[a-zA-Z0-9]){4,18}[a-zA-Z0-9]$')]],
      Email:['',[Validators.required,Validators.pattern('^[a-z0-9](\.?[a-z0-9]){5,}@g(oogle)?mail\.com$')]],
       Password:['',[Validators.required,Validators.pattern('^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{7,15}$')]],
      
    })
   }
  ngOnInit(): void {
  }


  public handleError = (controlName: string, errorName: string) => {
    return this.signup.controls[controlName].hasError(errorName);
  }


  register() {
    if (!this.signup.controls['username'].valid) {
      alert('Please enter UserName')
      this.signup.controls['username'].reset()
      return
    }
    if (!this.signup.controls['Email'].valid) {
      alert('Please enter correct email')
      this.signup.controls['Email'].reset()
      return
    }

    if (!this.signup.controls['Password'].valid) {
      alert('Please enter correct password format')
      this.signup.controls['Password'].reset()
      return
    }


      this.auth.saveuser(this.signup.value)
    
    
    this.auth.signup(this.signup.controls['Email'].value, this.signup.controls['Password'].value)
  .then(() => {
    this.router.navigateByUrl('/login')
    this.openSnackBar('You are signup sucessfully')
   }).catch(_error => {
     this.error = _error
     this.router.navigateByUrl('/signin')
     this.openSnackBar('Error Accour')

   })

  }



  openSnackBar(message:string) {
    this._snackBar.open(message,"ok", {
      duration: 1000,
    });
  }


}
