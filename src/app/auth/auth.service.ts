import { Injectable, NgZone } from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import { AngularFireList } from '@angular/fire/database';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import {Router} from '@angular/router';
import { User  } from './data';
import firebase from 'firebase/app';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  authState: any = null;
  auth :any;
  userData: any;

  userdetails!: AngularFireList<any> 
  items: any;

  constructor(   public ngZone: NgZone ,private afu: AngularFireAuth,public afs:AngularFirestore, private router: Router) { 

    this.afu.authState.subscribe((user:any) => {
      if (user) {
        this.userData = user;
        localStorage.setItem('user', JSON.stringify(this.userData));
        JSON.parse(localStorage.getItem('user')!);
      } else {
        localStorage.setItem('user', '');
      }
    })
  }

  AuthLogin(provider: any) {
    return this.afu.signInWithPopup(provider)
    .then((result:any) => {
       this.ngZone.run(() => {
          this.router.navigate(['home']);
        })
      this.SetUserData(result.user);
    }).catch((error:any) => {
      window.alert(error)
    })
  }

  googlelogin() {
    this.AuthLogin(new firebase.auth.GoogleAuthProvider());
  }
  

  get isLoggedIn(): boolean {
    const user = JSON.parse(localStorage.getItem('user')!);
    return (user !== null ) ? true : false;
  }

  ForgotPassword(passwordResetEmail: any) {
    return this.afu.sendPasswordResetEmail(passwordResetEmail)
    .then(() => {
      window.alert('Password reset email sent, check your inbox.');
    }).catch((error:any) => {
      window.alert(error)
    })
  }



signup(email: string, password: string) {
    return this.afu.createUserWithEmailAndPassword(email, password)
      .then((user:any) => {
        this.authState = user
      })
      .catch((error:any) => {
        console.log(error)
        throw error
      });
  }

  saveuser(User : User){
    this.afs.collection('user').add(User)
    .catch(error => {
      this.errorMgmt(error);
    })
  }

  

  getdata(){

     return this.afs.collection('user').snapshotChanges()
  }

  login(email: string, password: string)
  {
    return this.afu.signInWithEmailAndPassword(email, password)
      .then((user:any) => {
        this.authState = user
         console.log(this.authState)
      })
      .catch((error:string) => {
        console.log(error)
        throw error
      });
  }

  SetUserData(user:any) {
    const userRef: AngularFirestoreDocument<any> = this.afs!.doc(`user/${user.uid}`);
    const userData: User|any = {
      uid: user.uid,
      email: user.email,
      emailVerified: user.emailVerified
    }
    return userRef.set(userData, {
      merge: true
    })
  }

  
  
  
  singout(): void
  {
    this.afu.signOut();
    this.router.navigate(['/login']);
  }

  private errorMgmt(error:any) {
    console.log(error)
  }


}
